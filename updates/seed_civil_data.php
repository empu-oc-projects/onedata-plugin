<?php

namespace Empu\OneData\Updates;

use Empu\OneData\Models\BloodType;
use Empu\OneData\Models\EducationDegree;
use Empu\OneData\Models\Gender;
use Empu\OneData\Models\Marital;
use Empu\OneData\Models\Occupation;
use Empu\OneData\Models\Religion;
use Seeder;

class SeedCivilData extends Seeder
{
    public function run()
    {
        $this->seedBloodTypes();
        $this->seedEducationDegrees();
        $this->seedGenders();
        $this->seedMaritals();
        $this->seedOccupations();
        $this->seedReligions();
    }

    protected function seedEducationDegrees()
    {
        $degrees = [
            'Tidak / Belum Sekolah', // -1
            'Belum Tamat SD / Sederajat', // 0
            'SD / Sederajat', // 1
            'SLTP / Sederajat', // 2
            'SLTA / Sederajat', // 3
            'Diploma I / II', // 4
            'Akademi / Diploma III / Sarjana Muda', // 5
            'Diploma IV / Strata I', // 6
            'Strata II', // 7
            'Strata III', // 8
        ];

        collect($degrees)
        ->map(function ($label, $order) {
            return compact('label', 'order');
        })
        ->mapInto(EducationDegree::class)
        ->map(function ($item) {
            $item->save();
        });
    }

    protected function seedReligions()
    {
        $religions = [
            'Islam', 'Kristen', 'Katholik', 'Hindu', 'Budha', 'Kong Hucu',
            'Kepercayaan Terhadap Tuhan YME'
        ];

        collect($religions)
        ->map(function ($label, $order) {
            return compact('label', 'order');
        })
        ->mapInto(Religion::class)
        ->map(function ($item) {
            $item->save();
        });
    }

    protected function seedGenders()
    {
        $genders = [
            'Laki-laki', 'Perempuan'
        ];

        collect($genders)
        ->map(function ($label, $order) {
            return compact('label', 'order');
        })
        ->mapInto(Gender::class)
        ->map(function ($item) {
            $item->save();
        });
    }

    protected function seedMaritals()
    {
        $maritals = [
            'Belum Kawin',
            'Kawin',
            'Cerai Hidup',
            'Cerai Mati',
        ];

        collect($maritals)
        ->map(function ($label, $order) {
            return compact('label', 'order');
        })
        ->mapInto(Marital::class)
        ->map(function ($item) {
            $item->save();
        });
    }

    protected function seedOccupations()
    {
        $path = plugins_path('empu/onedata/updates/csv/occupations.csv');
        
        $this->handleCvsFile($path, function ($data) {
            $attrs = [
                // 'code' => $data[0],
                'label' => $data[1],
            ];

            Occupation::create($attrs);
        });
    }

    protected function seedBloodTypes()
    {
        $bloodTypes = [
            'A', 'B', 'AB', 'O', 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+',  'O-'
        ];

        collect($bloodTypes)
        ->map(function ($label, $order) {
            return compact('label', 'order');
        })->push([
            'label' => 'Tidak Tahu',
            'order' => 99,
        ])
        ->mapInto(BloodType::class)
        ->map(function ($item) {
            $item->save();
        });
    }

    protected function handleCvsFile(string $path, \Closure $callback)
    {
        $handle = fopen($path, 'r');

        while (($data = fgetcsv($handle, 200, ','))) {
            $callback($data);
        }

        fclose($handle);
    }
}