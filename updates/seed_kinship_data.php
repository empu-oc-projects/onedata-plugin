<?php

namespace Empu\OneData\Updates;

use Empu\OneData\Models\Kinship;
use Seeder;

class SeedKinshipData extends Seeder
{
    public function run()
    {
        $kinships = [
            'Kepala keluarga', 
            'Suami',
            'Istri',
            'Anak',
            'Menantu',
            'Cucu',
            'Orang tua',
            'Mertua',
            'Famili lain',
            'Pembantu',
        ];

        collect($kinships)
        ->map(function ($label, $order) {
            return compact('label', 'order');
        })->push([
            'label' => 'Lainnya',
            'order' => 99,
        ])
        ->mapInto(Kinship::class)
        ->map(function ($item) {
            $item->save();
        });
    }
}