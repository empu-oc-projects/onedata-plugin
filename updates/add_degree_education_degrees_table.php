<?php namespace Empu\OneData\Updates;

use Empu\OneData\Models\EducationDegree;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddDegreeEducationDegreesTable extends Migration
{
    public function up()
    {
        Schema::table('empu_onedata_education_degrees', function (Blueprint $table) {
            $table->smallInteger('degree')->default(0)->after('label');
        });

        EducationDegree::all()->each(function ($row) {
            $row->degree = $row->id - 2;
            $row->save();
        });
    }
    
    public function down()
    {
        Schema::table('empu_onedata_education_degrees', function (Blueprint $table) {
            $table->dropColumn('degree');
        });
    }
}
