<?php namespace Empu\OneData\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRegionsTable extends Migration
{
    public function up()
    {
        Schema::create('empu_onedata_regions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->uuid('uuid')->unique();
            $table->string('label');
            $table->string('bps_code')->unique();
            $table->string('name');
            $table->smallInteger('level')
                ->comment('0: country, 1: provinces, 2: regencies/cities, 3: subdistricts / districts, 4: villages');
            $table->unsignedInteger('parent_id')->nullable();
            $table->boolean('is_available')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_onedata_regions');
    }
}
