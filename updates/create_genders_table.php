<?php namespace Empu\OneData\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGendersTable extends Migration
{
    public function up()
    {
        Schema::create('empu_onedata_genders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallIncrements('id');
            $table->uuid('uuid')->unique();
            $table->string('label');
            $table->integer('order');
            $table->boolean('is_available')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_onedata_genders');
    }
}
