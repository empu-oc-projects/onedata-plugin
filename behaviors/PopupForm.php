<?php

namespace Empu\OneData\Behaviors;

use Backend\Behaviors\FormController;
use Backend\Classes\ControllerBehavior;
use October\Rain\Exception\ApplicationException;

class PopupForm extends ControllerBehavior
{
    public function __construct($controller)
    {
        parent::__construct($controller);

        $satisfied = 
            $this->controller->isClassExtendedWith('Backend.Behaviors.ListController')
            && $this->controller->isClassExtendedWith('Backend.Behaviors.FormController');

        // Throw errof if behaviors if not already implemented
        if (! $satisfied) {
            throw new ApplicationException('ListController and FormController must be implemented in the controller.');
        }
    }

    public function index_onPopupForm()
    {
        $manageId = post('manage_id');

        try {
            $form = $this->controller->asExtension('FormController');

            if (empty($manageId)) {
                $context = post('manage_context', FormController::CONTEXT_CREATE);
                $model = $this->controller->formCreateModelObject();
                $model = $this->controller->formExtendModel($model) ?: $model;
            }
            else {
                $context = post('manage_context', FormController::CONTEXT_UPDATE);
                $model = $this->controller->formFindModelObject($manageId);
            }

            $form->initForm($model, $context);
            $title = $form->getConfig("{$context}[title]");
        }
        catch (\Exception $ex) {
            \Log::error($ex);

            throw new AjaxException(['error' => 'Error occured during operations.']);
        }

        return $this->makePartial('popup', compact('model', 'title'));
    }

    public function index_onCreate()
    {
        return $this->controller->asExtension('FormController')->create_onSave();
    }

    public function index_onUpdate()
    {
        $manageId = post('manage_id');

        return $this->controller->asExtension('FormController')->update_onSave($manageId);
    }

    public function index_onDelete()
    {
        $manageId = post('manage_id');

        return $this->controller->asExtension('FormController')->update_onDelete($manageId);
    }
}