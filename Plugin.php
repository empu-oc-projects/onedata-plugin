<?php

namespace Empu\OneData;

use Backend;
use Empu\OneData\Libs\NINumberRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use System\Classes\PluginBase;

/**
 * OneData Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array Plugin dependencies
     */
    public $require = ['Empu.Support'];

    private $masterData = [
        'bloodtypes' => ['icon' => 'icon-tint'],
        'educationdegrees' => ['icon' => 'icon-graduation-cap'],
        'genders' => ['icon' => 'icon-venus-mars'],
        'kinships' => ['icon' => 'icon-address-card-o'],
        'maritals' => ['icon' => 'icon-heart-o'],
        'occupations' => ['icon' => 'icon-briefcase'],
        'regions' => ['icon' => 'icon-globe'],
        'religions' => ['icon' => 'icon-book'],
        // 'nationalities' => ['icon' => 'icon-flag'],
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'empu.onedata::lang.app.name',
            'description' => 'empu.onedata::lang.app.description',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-database'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        Rule::macro('validNIN', function () {
            return new NINumberRule();
        });
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Validator::extend('valid_nin', function($attribute, $value, $parameters, $validator) {
            return (new NINumberRule)->passes($attribute, $value);
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\OneData\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        $permissions = collect($this->masterData)->mapWithKeys(function ($props, $key) {
            return [
                "empu.onedata.manage_{$key}" => [
                    'tab' => 'empu.onedata::lang.app.name',
                    'label' => "empu.onedata::lang.permissions.manage_{$key}",
                ],
            ];
        });

        return $permissions->toArray();
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [];
    }

    public function registerSettings()
    {
        $order = 300;

        $settings = collect($this->masterData)->mapWithKeys(
            function ($props, $key) use (&$order) {
                return [
                    $key => [
                        'label'       => "empu.onedata::lang.settings.{$key}",
                        'description' => "empu.onedata::lang.settings.{$key}_description",
                        'category'    => 'empu.onedata::lang.app.name',
                        'icon'        => $props['icon'],
                        'url'         => Backend::url("empu/onedata/{$key}"),
                        'order'       => $order++,
                        'keywords'    => "settings master onedata data {$key}",
                        'permissions' => ["qodr.minori.manage_{$key}"],
                    ],
                ];
            }
        );

        return $settings->toArray();
    }
}
