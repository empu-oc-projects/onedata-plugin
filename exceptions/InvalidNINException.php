<?php

namespace Empu\OneData\Exceptions;

use Empu\Support\Exceptions\InvalidInputException;

/**
 * Invalid National Identification Number Exception
 */
class InvalidNINException extends InvalidInputException {}