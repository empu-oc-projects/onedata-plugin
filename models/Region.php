<?php

namespace Empu\OneData\Models;

use Model;
use October\Rain\Database\Builder;
use October\Rain\Support\Facades\Str;

/**
 * Region Model
 */
class Region extends Model
{
    use \Empu\Support\Behaviors\WithRefTrait;
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    const LEVEL_COUNTRY = 0;
    const LEVEL_PROVINCE = 1;
    const LEVEL_CITY = 2;
    const LEVEL_DISTRICT = 3;
    const LEVEL_VILLAGE = 4;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'empu_onedata_regions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $refKey = 'uuid';

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'children' => [
            self::class, 'key' => 'parent_id',
        ]
    ];
    public $belongsTo = [
        'parent' => [
            self::class, 'key' => 'parent_id',
        ]
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * {@inheritDoc}
     */
    public function beforeCreate()
    {
        $this->generateRef();
    }

    public function scopeIsAvailable(Builder $builder, $availability = true)
    {
        return $builder->where('is_available', $availability);
    }

    public function getLabelAttribute($value)
    {
        if (preg_match('~^KABUPATEN\s([\w\s]+)(,\s[.+])?$~i', $value, $matches)) {
            $value = $matches[1];
        }

        return Str::title($value);
    }

    public function scopeByBpsCode(Builder $builder, string $code)
    {
        return $builder->where('bps_code', $code);
    }

    public function scopeProvincesOnly(Builder $builder)
    {
        return $this->scopeByLevel($builder, self::LEVEL_PROVINCE);
    }

    public function scopeCitiesOnly(Builder $builder)
    {
        return $this->scopeByLevel($builder, self::LEVEL_CITY);
    }

    public function scopeDistrictsOnly(Builder $builder)
    {
        return $this->scopeByLevel($builder, self::LEVEL_DISTRICT);
    }

    public function scopeVillagesOnly(Builder $builder)
    {
        return $this->scopeByLevel($builder, self::LEVEL_VILLAGE);
    }

    public function scopeByLevel(Builder $builder, int $level)
    {
        return $builder->where('level', $level);
    }

    public function scopeGetAsOptions(Builder $builder)
    {
        return $builder->isAvailable()->pluck('label', 'uuid');
    }
}
