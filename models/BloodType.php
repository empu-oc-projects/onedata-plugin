<?php namespace Empu\OneData\Models;

use Model;
use October\Rain\Database\Builder;

/**
 * BloodType Model
 */
class BloodType extends Model
{
    use \Empu\Support\Behaviors\WithRefTrait;
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'empu_onedata_blood_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $refKey = 'uuid';

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * {@inheritDoc}
     */
    public function beforeCreate()
    {
        $this->generateRef();
        
        if (is_null($this->order)) {
            $this->order = 50;
        }
    }

    public function scopeIsAvailable(Builder $builder, $availability = true)
    {
        return $builder->where('is_available', $availability);
    }

    public function scopeGetAsOptions(Builder $builder)
    {
        return $builder->isAvailable()->pluck('label', 'uuid');
    }
}
