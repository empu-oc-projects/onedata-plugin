<?php

namespace Empu\OneData\Models;

use Model;
use October\Rain\Database\Builder;

/**
 * Kinship Model
 */
class Kinship extends Model
{
    use \Empu\Support\Behaviors\WithRefTrait;
    use \October\Rain\Database\Traits\SoftDelete;
    // use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'empu_onedata_kinships';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    protected $refKey = 'uuid';

    /**
     * {@inheritDoc}
     */
    public function beforeCreate()
    {
        $this->generateRef();
        
        if (is_null($this->order)) {
            $this->order = 50;
        }
    }

    public function scopeIsAvailable(Builder $builder, $availability = true)
    {
        return $builder->where('is_available', $availability);
    }

    public function scopeGetAsOptions(Builder $builder)
    {
        return $builder->isAvailable()->pluck('label', 'uuid');
    }
}
