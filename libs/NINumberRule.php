<?php

namespace Empu\OneData\Libs;

use Empu\OneData\Exceptions\InvalidNINException;
use Illuminate\Contracts\Validation\Rule;

class NINumberRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $pieces = NINomenclature::parse($value);
        }
        catch (InvalidNINException $th) {
            return false;
        }

        return join('', $pieces) == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute value not valid.';
    }
}
