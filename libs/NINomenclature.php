<?php

namespace Empu\OneData\Libs;

use Empu\OneData\Exceptions\InvalidNINException;

/**
 * National Identification Nomenclature
 */
class NINomenclature
{
    public static function tidyUp($number)
    {
        return preg_replace('~[^\d]~', '', $number);
    }

    public static function parse($number)
    {
        $tidyNumber = self::tidyUp($number);
        $pattern = '~^(\d{6})([012456][0-9]|[37][01])(0[1-9]|1[012])(\d{2})(\d{4})$~';
        $good = preg_match($pattern, $tidyNumber, $matches);

        if (! $good) {
            throw new InvalidNINException();
        }

        array_shift($matches);

        return $matches;
    }
}
